class Article {
  final String text;
  final String url;
  final String by;
  final String time;
  final int score;
  final int commentsCount;

  const Article(
      {this.text,
      this.url,
      this.by,
      this.time,
      this.score,
      this.commentsCount});


      factory Article.fromJson(Map<String, dynamic> json){
        if(json == null) return null;
        return Article(
          text:json['text'] ?? '[null]',
          url: json['url'],
          by: json['by'],
          time: json['time'] ,
          score: json['score'],
        );
      }
}

final articles = [
  new Article(
      text: "Title1: Harley-Davidson’s e-motorcycle debut and EV pivot",
      url: "turuu.com",
      by: "turuu",
      time: "1 week",
      score: 5,
      commentsCount: 0),
  new Article(
      text: "Title2: iOS 13: Here are the new security and privacy features you might’ve missed",
      url: "turuu.com",
      by: "turuu",
      time: "2 week",
      score: 12,
      commentsCount: 0),
  new Article(
      text: "Title3: Haus, the real estate startup founded by Garrett Camp, raises \$7.1M",
      url: "turuu.com",
      by: "turuu",
      time: "3 week",
      score: 45,
      commentsCount: 1),
  new Article(
      text: "Title4: Inside Harley-Davidson’s EV shift with a ride on its LiveWire",
      url: "turuu.com",
      by: "turuu",
      time: "1 week",
      score: 5,
      commentsCount: 0),
  new Article(
      text: "Title5: How US national security agencies hold the internet hostage",
      url: "turuu.com",
      by: "turuu",
      time: "2 week",
      score: 12,
      commentsCount: 0),
  new Article(
      text: "Title6: WeWork CEO Adam Neumann has reportedly cashed out of over \$700 million ahead of its IPO",
      url: "turuu.com",
      by: "turuu",
      time: "3 week",
      score: 45,
      commentsCount: 15),
  new Article(
      text: "Title7: Chinese space station Tiangong-2 is about to burn up over the Pacific",
      url: "turuu.com",
      by: "turuu",
      time: "1 week",
      score: 5,
      commentsCount: 10),
  new Article(
      text: "Title8: Uber and Lyft drivers demand better pay, workplace protections and driver-led unions",
      url: "turuu.com",
      by: "turuu",
      time: "2 week",
      score: 52,
      commentsCount: 63),
  new Article(
      text: "Title9: Hardware startups take center stage for Hardware Battlefield at TC Shenzhen",
      url: "turuu.com",
      by: "turuu",
      time: "3 week",
      score: 96,
      commentsCount: 145),
];
