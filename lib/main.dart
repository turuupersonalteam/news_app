import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'src/article.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter news app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Turuu news app'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Article> _articles = articles;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        // scrollDirection: Axis.horizontal,
        children: _articles.map(_buildItem).toList(),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _buildItem(Article a) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListTile(
        title: Text(
          a.text,
          style: TextStyle(fontSize: 18),
        ),
        subtitle: Text("${a.commentsCount} comments"),
        onTap: () async{
          final urlString = "http://${a.url}";
          if(await canLaunch("http://${a.url}")){
            launch(urlString);
          }
        },
      ),
    );
  }
}
